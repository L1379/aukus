"""Aukus rules implementation."""
from enum import IntEnum
from itertools import product
from typing import NamedTuple
from collections.abc import Callable

import numpy as np

Array = np.ndarray
NONE_TILE = -1
HOURS_IN_DAY = 24.
_CHAINED_SNAKE_ERROR = 'Chained snakes/ladders should be reduced.'


class GameDuration(IntEnum):
    """The only game related parameter. A longer game allows to roll more dice."""
    Short = 0
    Normal = 1
    Long = 2


class Model(NamedTuple):
    """The game parametrization.

    Args:
        num_tiles -- the last tile number, starting position is equal to zero.
        snakes_and_ladders -- 1d array of snakes endings. NONE_TILE if there is no snake or ladder.
        endgame_tile -- rules are harsher after this tile.
        allow_watch_every -- completed games streak required to unlock an additional action.
        game_duration_probs -- probabilities to sample a game from each of GameDuration category.
    """
    num_tiles: int
    snakes_and_ladders: Array
    endgame_tile: int
    allow_watch_every: int
    game_duration_probs: Array

    def validate(self):
        """Common sense checks."""
        assert self.snakes_and_ladders.shape == (self.num_tiles + 1,)
        assert np.all(self.snakes_and_ladders < self.num_tiles)
        assert self.allow_watch_every > 0
        assert self.game_duration_probs.shape == (len(GameDuration),) and self.game_duration_probs.sum() == 1.


class State(NamedTuple):
    """Complete Markovian state definition of the Aukus. This should be enough to take an informed decision.
    However, does not take into account other players or detailed game description.

    Args:
        rng -- stateful random number generator.
        pos -- position on the board.
        game_duration -- time required to complete current game.
        games_streak -- current games streak.
    """
    rng: np.random.MT19937
    pos: int
    game_duration: GameDuration
    games_streak: int


class Action(IntEnum):
    """Possible actions."""
    DROP = 0
    MOVE_1d6 = 1
    MOVE_2d6 = 2
    MOVE_3d6 = 3  # Disabled by default, used for an ablations study.
    WATCH = 4

    @property
    def move_direction(self) -> int:
        if self == Action.DROP:
            return -1
        return 1

    def is_move(self) -> bool:
        return self in (Action.MOVE_1d6, Action.MOVE_2d6, Action.MOVE_3d6)


Policy = Callable[[State], Action]


def legal_actions(model: Model,
                  state: State,
                  ) -> list[Action]:
    """List possible action from the state."""
    actions = [Action.DROP, Action.MOVE_1d6]
    if state.pos < model.endgame_tile:
        if state.game_duration >= GameDuration.Normal:
            actions.append(Action.MOVE_2d6)
        if state.game_duration >= GameDuration.Long:
            actions.append(Action.MOVE_3d6)
    if state.games_streak == model.allow_watch_every:
        actions.append(Action.WATCH)
    return actions


def move_player(model: Model,
                state: State,
                raw_move: int
                ) -> int:
    """Comply the Aukus rules."""
    pos = np.clip(state.pos + raw_move, 0, model.num_tiles)
    snl = model.snakes_and_ladders
    if (snaked := snl[pos]) != NONE_TILE:
        assert snl[snaked] == NONE_TILE, _CHAINED_SNAKE_ERROR
        pos = snaked
    return pos


def update_streak(model: Model,
                  state: State,
                  action: Action) -> int:
    """Update games streak according to the rules.
    If Action.WATCH is not taken, the counter still resets."""
    streak = state.games_streak % model.allow_watch_every
    return action.is_move() * (streak + 1)


def get_reward(model: Model, state: State, action: Action) -> float:
    """Convention is that reward is correlated with a real time requirement to perform an action."""
    time_cost = {  # in hours
        'DROP': 1,
        GameDuration.Short: 8,
        GameDuration.Normal: 16,
        GameDuration.Long: 24,
        'WATCH': 2
    }
    match action:
        case Action.DROP:
            cost = time_cost['DROP']
        case Action.MOVE_1d6 | Action.MOVE_2d6 | Action.MOVE_3d6:
            cost = time_cost[state.game_duration]
        case Action.WATCH:
            cost = time_cost['WATCH']
        case _:
            raise ValueError(action)
    return - cost / HOURS_IN_DAY


def get_termination(model: Model, state: State) -> bool:
    """The rules also require to finish a game on the last tile,
    but there is no optionality to consider, so just move there."""
    return state.pos == model.num_tiles


def get_chances(dice: list[int]) -> Array:
    """Compute events probabilities. Zero event is also padded to ease indexing."""
    max_event, cardinality = sum(dice), np.prod(dice)
    events = np.zeros(max_event + 1, np.int32)
    rolls = map(lambda d: range(1, d + 1), dice)
    for event in product(*rolls):
        events[sum(event)] += 1
    return events / cardinality


# The probabilities can be computed once and stored but whatever.
_probs_1d4 = get_chances([4])
_probs_1d6 = get_chances([6])
_probs_2d6 = get_chances([6, 6])
_probs_3d6 = get_chances([6, 6, 6])


def expand_chance_node(model: Model,
                       state: State,
                       action: Action
                       ) -> tuple[Array, ...]:
    """There is an uncertainty in the game. This enumerates random events probabilities."""
    match action:
        case Action.DROP:
            roll = _probs_1d6 if state.pos < model.endgame_tile else _probs_2d6
        case Action.MOVE_1d6:
            roll = _probs_1d6
        case Action.MOVE_2d6:
            roll = _probs_2d6
        case Action.MOVE_3d6:
            roll = _probs_3d6
        case Action.WATCH:
            roll = _probs_1d4
        case _:
            raise ValueError(action)
    return roll, model.game_duration_probs


def dict_to_contiguous(snakes_and_ladders: dict[int, int],
                       num_tiles: int
                       ) -> Array:
    """It is more convenient to input snakes as a dict. Thus, here is this helper function."""
    snl = np.full(num_tiles + 1, NONE_TILE, np.int32)
    for begin_, end_ in snakes_and_ladders.items():
        if 0 < min(begin_, end_) and max(begin_, end_) < num_tiles:
            snl[begin_] = end_
    return snl


def get_default_snakes_and_ladders():
    """In accordance to the Aukus board."""
    return {
        1: 20,
        4: 25,
        13: 46,
        21: 3,
        27: 5,
        33: 49,
        42: 63,
        43: 18,
        44: 68,
        47: 11,
        50: 69,
        54: 31,
        62: 81,
        66: 45,
        71: 90,
        74: 92,
        76: 58,
        89: 53,
        94: 67,
        96: 84,
        97: 85,
        99: 41
    }
