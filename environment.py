from collections import defaultdict
from typing import Any

import numpy as np

import aukus


class Aukus:
    """Wraps the game rules into a single class."""

    def __init__(self,
                 num_tiles: int = 101,
                 snakes_and_ladders: dict[int, int] = aukus.get_default_snakes_and_ladders(),
                 endgame_tile: int = 81,
                 allow_watch_every: int = 5,
                 game_duration_probs: tuple[float, ...] = (0.5, 0.5, 0.),
                 ) -> None:
        """The fields are the same as in the aukus.Model.
        Additional assumption is made that short and normal games are equiprobable.
        """
        self.snakes_and_ladders = snakes_and_ladders.copy()
        snl = aukus.dict_to_contiguous(snakes_and_ladders, num_tiles)
        self.model = aukus.Model(num_tiles=num_tiles,
                                 snakes_and_ladders=snl,
                                 endgame_tile=endgame_tile,
                                 allow_watch_every=allow_watch_every,
                                 game_duration_probs=np.asarray(game_duration_probs)
                                 )
        self.model.validate()

    def reset(self, seed: int) -> aukus.State:
        """Prepare a new state."""
        bit_generator = np.random.MT19937(seed=seed)
        rng = np.random.Generator(bit_generator)
        game_dur = rng.choice(aukus.GameDuration, p=self.model.game_duration_probs)
        return aukus.State(
            rng=bit_generator,
            pos=0,
            game_duration=aukus.GameDuration(game_dur),
            games_streak=0,
        )

    def step(self, state: aukus.State, action: aukus.Action) -> tuple[aukus.State, float, bool, dict[str, Any]]:
        """Advance the state."""
        action = aukus.Action(action)
        if action not in aukus.legal_actions(self.model, state):
            raise RuntimeError('Invalid action.')
        rng = np.random.Generator(state.rng.jumped())
        roll_probs, game_dur_probs = aukus.expand_chance_node(self.model, state, action)
        move = action.move_direction * rng.choice(len(roll_probs), p=roll_probs)
        # Meta info should not be considered by the agent. It is only for game analysis.
        raw_move = state.pos + move
        is_snake_activated = raw_move in self.snakes_and_ladders
        meta = dict(
            snake_activated=raw_move if is_snake_activated else aukus.NONE_TILE,
            snake_pos_diff=self.snakes_and_ladders[raw_move] - raw_move if is_snake_activated else 0
        )
        # end of meta.
        pos = aukus.move_player(self.model, state, move)
        game_dur = rng.choice(aukus.GameDuration, p=game_dur_probs)
        games_streak = aukus.update_streak(self.model, state, action)
        reward = aukus.get_reward(self.model, state, action)
        next_state = aukus.State(
            rng=rng.bit_generator,
            pos=pos,
            game_duration=aukus.GameDuration(game_dur),
            games_streak=games_streak,
        )
        is_terminal = aukus.get_termination(self.model, next_state)
        return next_state, reward, is_terminal, meta

    def legal_actions(self, state: aukus.State) -> list[aukus.Action]:
        return aukus.legal_actions(self.model, state)

    def is_terminal(self, state: aukus.State) -> bool:
        return aukus.get_termination(self.model, state)

    def evaluate_policy(self, policy: aukus.Policy, num_trajectories: int = 10_000) -> dict[str, list[Any]]:
        """"""
        summary = defaultdict(list)
        for i in range(num_trajectories):
            trajectory = environment_loop(i, self, policy)
            summary['positions'].extend([s.pos for s in trajectory['states']])
            summary['actions'].extend(trajectory['actions'])
            summary['snakes_and_ladders'].extend(trajectory['snakes'])
            summary['snakes_pos_diff'].append(sum(trajectory['snakes_pos_diff']))
            summary['returns'].append(sum(trajectory['rewards']))
        summary = {k: np.asarray(v) for k, v in summary.items()}
        return summary


def environment_loop(seed: int,
                     env: Aukus,
                     policy: aukus.Policy
                     ) -> dict[str, list[Any]]:
    """Simple environment loop for policy evaluation."""
    state = env.reset(seed=seed)
    trajectory = defaultdict(list)
    is_terminal = False
    while not is_terminal:
        action = policy(state)
        new_state, reward, is_terminal, meta = env.step(state, action)
        trajectory['states'].append(state)
        trajectory['actions'].append(action)
        trajectory['rewards'].append(reward)
        snake = meta['snake_activated']
        if snake != aukus.NONE_TILE:
            trajectory['snakes'].append(snake)
            trajectory['snakes_pos_diff'].append(meta['snake_pos_diff'])
        state = new_state
    trajectory['states'].append(state)
    return trajectory
