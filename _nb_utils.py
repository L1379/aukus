"""Helper functions for the notebook. Feel free to skip this."""
import numpy as np
from scipy.stats import bootstrap
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.patches as mpp
from PIL import Image

import aukus

np.set_printoptions(precision=3)


def draw_canvas():
    img = Image.open('media/board.jpg')
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(img, alpha=.6)
    ax.grid(visible=True, which='both', axis='both', color='k', linewidth=1.5)
    ax.set_xticks(np.arange(0, 1000, 100), '')
    ax.set_yticks(np.arange(0, 1000, 100), '')
    return fig, ax


def draw_patch(ax, idx, action):
    y, x = np.unravel_index(idx - 1, (10, 10))
    inv = bool(y % 2)
    x = 100 * (x + 1) - 50
    if inv:
        x = 1000 - x
    y = 950 - 100 * y
    x_off = -40 if inv else 40
    dx = -80 if inv else 80
    def arrow(x, y, dx, color='green'): return mpp.Arrow(x, y, dx, 0, width=20, color=color)
    match action:
        case float():
            m = cm.ScalarMappable(norm=mpl.colors.Normalize(vmin=-30, vmax=0), cmap=cm.RdYlGn)
            color = m.to_rgba(action)
            color = list(color)
            color[-1] = .5
            el = mpp.Rectangle((x - 50, y - 50), 100, 100, color=tuple(color))
            ax.add_patch(el)
        case aukus.Action.DROP:
            el = arrow(x + x_off, y, -dx, color='red')
            ax.add_patch(el)
        case aukus.Action.MOVE_1d6:
            el = arrow(x - x_off, y, dx)
            ax.add_patch(el)
        case aukus.Action.MOVE_2d6:
            el = arrow(x - x_off, y - 20, dx)
            ax.add_patch(el)
            el = arrow(x - x_off, y + 20, dx)
            ax.add_patch(el)
        case aukus.Action.MOVE_3d6:
            el = arrow(x - x_off, y - 30, dx)
            ax.add_patch(el)
            el = arrow(x - x_off, y + 30, dx)
            ax.add_patch(el)
            el = arrow(x - x_off, y, dx)
            ax.add_patch(el)
        case aukus.Action.WATCH:
            el = mpp.Circle((x, y), 25, color='green', fill=False, linewidth=3)
            ax.add_patch(el)
        case _:
            raise ValueError(action)


def viz_slice(env: 'Aukus', policy: aukus.Policy, game_duration: aukus.GameDuration, games_streak: int) -> None:
    fig, ax = draw_canvas()
    for pos in range(1, env.model.num_tiles):
        if pos in env.snakes_and_ladders:
            continue
        state = aukus.State(rng=None, pos=pos, game_duration=game_duration, games_streak=games_streak)
        action = policy(state)
        draw_patch(ax, pos, action)
    ax.text(1010, 50, f'Slice:\ngame_duration={game_duration.name}\ngames_streak={games_streak}')


def viz_summary(summary):
    summary = summary.copy()
    snakes_pos_diff = summary.pop('snakes_pos_diff')
    n = len(summary)
    size = 10
    fig, axes = plt.subplots(1, n, figsize=(n  * size, size))
    axes[0].set_ylabel('probability')
    duration = -summary.pop('returns')
    for ax, (metric, values) in zip(axes, summary.items()):
        ax.set_title(metric)
        vals, counts = np.unique(values, return_counts=True)
        counts = counts / counts.sum()
        bar = ax.bar(vals, counts, align='center')
        if metric == 'actions':
            named_actions = list(map(lambda a: a.name, aukus.Action))
            ax.set_xticks(list(aukus.Action))
            ax.set_xticklabels(named_actions, rotation='vertical')
        elif metric == 'snakes_and_ladders':
            ax.set_xticks(vals)
            ax.set_xticklabels(vals, fontsize=8)
            bar.set_label(f'Mean s&l pos movement={np.mean(snakes_pos_diff): .2f}')
            ax.legend()
    ax = axes[-1]
    ax.set_title('Time in days')
    probs = (.05, .50, .95)
    quantiles = np.quantile(duration, probs)
    label = '\n'.join(map(lambda x: f'{x[0] : .2f} quantile = {x[1] : .2f}', zip(probs, quantiles)))
    ax.hist(duration, density=True, label=label)
    ax.legend()
    plt.show()


def viz_n_players_returns(data, ns):
    y_lb, y, y_ub = [], [], []
    ns = list(ns)
    for n in ns:
        res = bootstrap((data,), lambda x: np.min(x[:n]), method='percentile', n_resamples=10**5)
        ci = res.confidence_interval
        y_lb.append(ci.low)
        y.append(res.bootstrap_distribution.mean())
        y_ub.append(ci.high)
    plt.title('Event duration as a function of number of players.')
    plt.plot(ns, y)
    plt.fill_between(ns, y_lb, y_ub, alpha=0.2)
    plt.xlabel('Number of players')
    plt.ylabel('Time')
