from dataclasses import dataclass, field
from itertools import product
import warnings

import numpy as np

import aukus
from environment import Aukus


@dataclass(kw_only=True, frozen=True)
class Solver:

    @dataclass
    class Summary:
        qvalues: aukus.Array = field(repr=False)
        iterations: int
        update_error: float
        bellman_error: float

    discount: float = 1.
    abs_error: float = 1e-4
    max_iterations: int = float('inf')
    min_value: float = -200.
    fast_max: bool = False
    state_aliasing: bool = True
    dtype: np.floating = np.float32

    def __call__(self, env: Aukus) -> Summary:
        model = env.model
        tiles, streaks, duration = model.num_tiles + 1, model.allow_watch_every + 1, len(aukus.GameDuration)
        shape = (len(aukus.Action), tiles, streaks, duration)
        qvalues = np.full(shape, self.min_value, self.dtype)
        qvalues[:, -1] = 0
        a_err, b_err, iterations = float('inf'), 0., 0
        while max(a_err, b_err) > self.abs_error and iterations < self.max_iterations:
            iterations += 1
            b_err = 0
            prev_qvalues = qvalues.copy()
            all_states = product(reversed(range(tiles - 1)), range(streaks), range(duration))
            for pos, streak, game_dur in all_states:
                state = aukus.State(rng=None, pos=pos, game_duration=aukus.GameDuration(game_dur), games_streak=streak)
                for action in aukus.legal_actions(model, state):
                    dice_roll, game_is_long = aukus.expand_chance_node(model, state, action)
                    direction = action.move_direction
                    next_poses = [aukus.move_player(model, state, direction * i) for i, _ in enumerate(dice_roll)]
                    next_streak = aukus.update_streak(model, state, action)
                    if self.fast_max:
                        # Should be enough for most purposes.
                        # If a proper min_value is provided then illegal actions are never selected.
                        assert not self.state_aliasing, "fast_max can't be used together with state-aliasing."
                        next_val = qvalues[:, next_poses, next_streak].max(0)
                    else:
                        # Exact maximization w/ legal_actions and state aliasing.
                        def maybe_alias(action_, pos_, streak_, dur_):
                            if self.state_aliasing and action_ == aukus.Action.WATCH:
                                return qvalues[aukus.Action.WATCH, pos_, streak_] @ model.game_duration_probs
                            return qvalues[action_, pos_, streak_, dur_]

                        next_val = np.zeros((len(next_poses), len(aukus.GameDuration)), dtype=self.dtype)
                        events = product(enumerate(next_poses), enumerate(aukus.GameDuration))
                        for (i, next_pos), (j, next_dur) in events:
                            next_state = aukus.State(rng=None, pos=next_pos,
                                                     game_duration=next_dur, games_streak=next_streak)
                            next_val[i, j] = max(maybe_alias(next_action, next_pos, next_streak, next_dur)
                                                 for next_action in aukus.legal_actions(model, next_state))
                    reward = aukus.get_reward(model, state, action)
                    target_val = reward + self.discount * (dice_roll @ next_val @ game_is_long)
                    val = qvalues[action, pos, streak, game_dur]
                    b_err = max(abs(target_val - val), b_err)
                    qvalues[action, pos, streak, game_dur] = target_val
            a_err = np.abs(qvalues - prev_qvalues).max()
        if self.fast_max and np.any(qvalues < self.min_value):
            warnings.warn('The solution contains values lesser then the provided minimum value.')
        qvalues = np.where(qvalues == self.min_value, np.finfo(qvalues.dtype).min, qvalues)
        return Solver.Summary(
            qvalues=qvalues,
            iterations=iterations,
            update_error=a_err,
            bellman_error=b_err
        )

    def infer_policy(self, input_: Aukus | Summary | aukus.Array) -> aukus.Policy:
        match input_:
            case Aukus():
                qvalues = self(input_).qvalues
            case Solver.Summary():
                qvalues = input_.qvalues
            case _:
                qvalues = input_

        def policy(state):
            pos = state.pos
            game_dur = state.game_duration
            streak = state.games_streak
            action = qvalues[:, pos, streak, game_dur].argmax()
            return aukus.Action(action)
        return policy
